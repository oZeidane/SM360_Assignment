package com.sm360.assignment.advertising.infrastructure.repositories;

import com.sm360.assignment.advertising.domain.model.Dealer;
import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;
import com.sm360.assignment.advertising.infrastructure.entities.DealerEntity;
import com.sm360.assignment.advertising.infrastructure.entities.ListingEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListingRepositoryAdapterTest {

  @Mock
  private ListingRepository listingRepository;

  @Mock
  private DealerRepository dealerRepository;

  @InjectMocks
  private ListingRepositoryAdapter adapter;

  @Test
  @DisplayName("Save test case: with a existant dealer should get the already existant dealer data")
  void save_withExistantDealer() {
    // GIVEN
    final UUID idDealer = UUID.randomUUID();
    final String dealerName = "dealerName";
    final Listing listing = Listing.builder()
            .dealer(Dealer.builder().id(idDealer).name(dealerName).build())
            .build();

    when(dealerRepository.findById(idDealer)).thenReturn(Optional.of(DealerEntity.builder().id(idDealer).name(dealerName).build()));

    // WHEN
    adapter.save(listing);

    // THEN
    verify(dealerRepository).findById(idDealer);
    verify(dealerRepository, new Times(0)).findByName(any());
    verify(listingRepository).save(any());
  }

  @Test
  @DisplayName("Save test case: with a existant dealer name with no id should get the already existant dealer data")
  void save_withExistantDealerName() {
    // GIVEN
    final String dealerName = "dealerName";
    final Listing listing = Listing.builder()
            .dealer(Dealer.builder().name(dealerName).build())
            .build();

    when(dealerRepository.findByName(dealerName)).thenReturn(Optional.of(DealerEntity.builder().name(dealerName).build()));

    // WHEN
    adapter.save(listing);

    // THEN
    verify(dealerRepository, new Times(0)).findById(any());
    verify(dealerRepository).findByName(dealerName);
    verify(listingRepository).save(any());
  }

  @ParameterizedTest
  @EnumSource(State.class)
  @DisplayName("findByDealerNameAndState test case: should call the infrastructure service to get the list and map the Domain objects")
  void findByDealerNameAndState(State state) {
    // GIVEN
    final UUID idDealer = UUID.randomUUID();
    final String dealerName = "dealerName";

    final UUID idListing = UUID.randomUUID();
    final ListingEntity expectedListingEntity = ListingEntity.builder()
            .id(idListing)
            .dealer(DealerEntity.builder().id(idDealer).name(dealerName).build())
            .build();

    when(listingRepository.findByDealerNameAndState(dealerName, state)).thenReturn(Collections.singletonList(expectedListingEntity));

    // WHEN
    final Collection<Listing> result = adapter.findByDealerNameAndState(dealerName, state);

    // THEN
    assertNotNull(result);
    assertEquals(result.stream().findFirst().get().getId(), idListing);
  }

  @ParameterizedTest
  @EnumSource(State.class)
  @DisplayName("getListingState test case: should call the infrastructure service to get the list and map the Domain objects")
  void getListingState(State state) {

    // GIVEN
    final UUID idListing = UUID.randomUUID();
    final ListingEntity expectedListingEntity = ListingEntity.builder()
            .id(idListing)
            .state(state)
            .build();
    when(listingRepository.findById(idListing)).thenReturn(Optional.of(expectedListingEntity));

    // WHEN
    final Optional<State> result = adapter.getListingState(idListing);

    // THEN
    assertTrue(result.isPresent());
    assertEquals(result.get(), state);
  }

  @Test
  @DisplayName("findByOrderByPublishedAtDesc test case: should call the infrastructure service to get the list and map the Domain objects")
  void findByOrderByPublishedAtDesc() {
    // GIVEN
    final UUID idDealer = UUID.randomUUID();
    final String dealerName = "dealerName";

    final UUID idListing = UUID.randomUUID();
    final ListingEntity expectedListingEntity = ListingEntity.builder()
            .id(idListing)
            .dealer(DealerEntity.builder().id(idDealer).name(dealerName).build())
            .build();
    when(listingRepository.getTopByDealerNameOrderByPublishedAtDesc(dealerName)).thenReturn(expectedListingEntity);

    // WHEN
    final Listing result = adapter.getOldestDealerPublishedListing(dealerName);

    // THEN
    assertNotNull(result);
    assertEquals(result.getId(), idListing);
    assertEquals(result.getDealer().id(), idDealer);
    assertEquals(result.getDealer().name(), dealerName);
  }

  @ParameterizedTest
  @EnumSource(State.class)
  @DisplayName("countByStateAndDealer test case: should call the infrastructure service")
  void countByStateAndDealer(State state) {
    // GIVEN
    final long expectedCount = 5L;
    final String dealerName = "dealerName";
    when(listingRepository.countByStateAndDealerName(state, dealerName)).thenReturn(expectedCount);

    // WHEN
    final long result = adapter.countByStateAndDealerName(state, dealerName);

    // THEN
    assertEquals(result, expectedCount);
  }
}