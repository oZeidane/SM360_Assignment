package com.sm360.assignment.advertising.application;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
class RestControllerListingTest {

  @Autowired
  private MockMvc mvc;

  @Test()
  @Order(1)
  @DisplayName("Creation test case: new Listing/dealer and get the data of the created items")
  void create() throws Exception {

    final String content = """
            {
              "dealer": {
                "name": "dealerTestName"
              },
              "vehicle": "V1",
              "price": 1000.5,
              "createdAt": "2022-10-13T22:28:09.422608"
            }
            """;

    mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content(content))
            .andExpect(status().isCreated());

    mvc.perform(get("/draft/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].vehicle", is("V1")),
                    jsonPath("$[0].price", is(1000.5)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].state", is("DRAFT"))
            );
  }

  @Test
  @Order(2)
  @DisplayName("publish test case: publish the item created before and call a the get publishedItem to verify that update")
  void publish() throws Exception {

    final AtomicReference<String> idListing = new AtomicReference<>();

    mvc.perform(get("/draft/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].state", is("DRAFT"))
            )
            .andDo(result -> idListing.set(JsonPath.read(result.getResponse().getContentAsString(), "$[0].id")));

    mvc.perform(put("/publish/{idListing}", idListing.get()))
            .andExpect(status().isAccepted());

    mvc.perform(get("/published/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].vehicle", is("V1")),
                    jsonPath("$[0].price", is(1000.5)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].state", is("PUBLISHED"))
            );

  }

  @Test
  @Order(3)
  @DisplayName("unPublish test case: unpublish the item published before and call a the get unpublishedItem to verify that update")
  void unPublish() throws Exception {

    final AtomicReference<String> idListing = new AtomicReference<>();

    mvc.perform(get("/published/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].state", is("PUBLISHED"))
            )
            .andDo(result -> idListing.set(JsonPath.read(result.getResponse().getContentAsString(), "$[0].id")));

    mvc.perform(put("/unpublish/{idListing}", idListing.get()))
            .andExpect(status().isAccepted());

    mvc.perform(get("/draft/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].vehicle", is("V1")),
                    jsonPath("$[0].price", is(1000.5)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].state", is("DRAFT"))
            );

  }

  @Test
  @Order(4)
  @DisplayName("update test case: update the item that we have and call a the get methode to verify that update")
  void update() throws Exception {

    final AtomicReference<String> idListing = new AtomicReference<>();

    mvc.perform(get("/draft/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].vehicle", is("V1")),
                    jsonPath("$[0].price", is(1000.5)),
                    jsonPath("$[0].state", is("DRAFT"))
            )
            .andDo(result -> idListing.set(JsonPath.read(result.getResponse().getContentAsString(), "$[0].id")));


    final String ListingUpdated = String.format("""
            {
              "id": "%s",
              "dealer": {
                "name": "dealerTestName"
              },
              "vehicle": "V1",
              "price": 2000.5,
              "createdAt": "2022-10-13T22:28:09.422608",
              "publishedAt": null,
              "state": "PUBLISHED"
            }
            """, idListing);

    mvc.perform(put("/").contentType(MediaType.APPLICATION_JSON).content(ListingUpdated))
            .andExpect(status().isAccepted());

    mvc.perform(get("/published/dealerTestName"))
            .andExpectAll(
                    content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON),
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].vehicle", is("V1")),
                    jsonPath("$[0].price", is(2000.5)),
                    jsonPath("$[0].state", is("PUBLISHED"))
            );
  }


  @Test
  @Order(5)
  @DisplayName("autoTierLimitUnPublish test case: make sure that we have a published item and create a new one and published it with the lime at 1 and the autoTierLimitUnPublish activated")
  void autoTierLimitUnPublish() throws Exception {

    final AtomicReference<String> idOldListing = new AtomicReference<>();

    mvc.perform(get("/published/dealerTestName"))
            .andExpectAll(
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].state", is("PUBLISHED"))
            )
            .andDo(result -> idOldListing.set(JsonPath.read(result.getResponse().getContentAsString(), "$[0].id")));



    mvc.perform(get("/published/dealerTestName"))
            .andExpectAll(
                    status().isOk(),
                    jsonPath("$[0].state", is("PUBLISHED"))
            );

    mvc.perform(get("/draft/dealerTestName"))
            .andExpect(
                    status().isNotFound()
            );

    // Add new Listing to the Dealer dealerTestName
    final String content = """
            {
              "dealer": {
                "name": "dealerTestName"
              },
              "vehicle": "V2",
              "price": 5000.5,
              "createdAt": "2022-10-13T22:28:09.422608"
            }
            """;

    mvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content(content))
            .andExpect(status().isCreated());

    final AtomicReference<String> idNewListing = new AtomicReference<>();
    mvc.perform(get("/draft/dealerTestName"))
            .andExpectAll(
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].dealer.name", is("dealerTestName")),
                    jsonPath("$[0].vehicle", is("V2")),
                    jsonPath("$[0].state", is("DRAFT"))
            )
            .andDo(result -> idNewListing.set(JsonPath.read(result.getResponse().getContentAsString(), "$[0].id")));

    // Publish the new item for dealerTestName and activate the autoTierLimitUnPublish to unpublich the old one

    mvc.perform(put("/publish/{idListing}", idNewListing.get()))
            .andExpect(status().isAccepted());

    mvc.perform(get("/published/dealerTestName"))
            .andExpectAll(
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].id", is(idNewListing.get())),
                    jsonPath("$[0].state", is("PUBLISHED"))
            );

    mvc.perform(get("/draft/dealerTestName"))
            .andExpectAll(
                    status().isOk(),
                    jsonPath("$", hasSize(1)),
                    jsonPath("$[0].id", is(idOldListing.get())),
                    jsonPath("$[0].state", is("DRAFT"))
            );
  }


  @Test
  @Order(0) // Order not important
  @DisplayName("NoItemFoundException test case: try to publish/UnPublish a unknown item")
  void publishAndUnPublishUnknownItem() throws Exception {
    mvc.perform(put("/unpublish/{idListing}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());

    mvc.perform(put("/publish/{idListing}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
  }

  @Test
  @Order(0) // Order not important
  @DisplayName("NoItemFoundException test case: try to call a get list of listing of a unknown dealer")
  void getDraftListingOfUnknownDealer() throws Exception {
    mvc.perform(get("/draft/UnknownDealerName"))
            .andExpect(status().isNotFound());

    mvc.perform(get("/published/UnknownDealerName"))
            .andExpect(status().isNotFound());
  }

}