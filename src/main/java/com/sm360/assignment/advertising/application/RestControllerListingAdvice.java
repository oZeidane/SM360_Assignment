package com.sm360.assignment.advertising.application;

import com.sm360.assignment.advertising.domain.exception.NoItemFoundException;
import com.sm360.assignment.advertising.domain.exception.TierLimitException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * Spring Class that map the exception with the http code wanted
 */
@RestControllerAdvice
public class RestControllerListingAdvice {

  @ExceptionHandler(NoItemFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String noItemFoundHandler(NoItemFoundException ex) {
    return ex.getMessage();
  }

  @ExceptionHandler(TierLimitException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  String tierLimitHandler(TierLimitException ex) {
    return ex.getMessage();
  }
}
