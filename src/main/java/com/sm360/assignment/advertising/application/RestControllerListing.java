package com.sm360.assignment.advertising.application;

import com.sm360.assignment.advertising.domain.api.IListingService;
import com.sm360.assignment.advertising.domain.exception.NoItemFoundException;
import com.sm360.assignment.advertising.domain.exception.TierLimitException;
import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * Entry point of the application using REST API
 */
@RestController
@AllArgsConstructor
public class RestControllerListing {

  private final IListingService service;

  /**
   * Create a new listing item in the app
   *
   * @param listing item to create
   */
  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping
  @Operation(summary = "Create a new listing item in the app")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "202", description = "Item created", content = @Content)})
  public void create(
          @Parameter(description = "item to create")
          @Validated
          @RequestBody
          Listing listing) {
    service.createListing(listing);
  }

  /**
   * Update an existing listing item in the app
   *
   * @param listing item to update
   *
   * @throws TierLimitException exception if the tierLimit is reached and the autoTierLimitUnPublish is off
   */
  @ResponseStatus(HttpStatus.ACCEPTED)
  @PutMapping
  @Operation(summary = "Update an existing listing item in the app")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "202", description = "Item updated", content = @Content),
          @ApiResponse(responseCode = "403", description = "Tier limit exceeded", content = @Content)})
  public void update(
          @Parameter(description = "item to update")
          @Validated
          @RequestBody
          Listing listing) throws TierLimitException {
    service.updateListing(listing);
  }

  /**
   * Publish an existing listing item = change status to published
   *
   * @param idListing id of the listing item
   *
   * @throws TierLimitException   exception if the tierLimit is reached and the autoTierLimitUnPublish is off
   * @throws NoItemFoundException exception if the item is not found
   */
  @ResponseStatus(HttpStatus.ACCEPTED)
  @PutMapping("publish/{idListing}")
  @Operation(summary = "Publish a existing listing item = change status to published")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "202", description = "Item published", content = @Content),
          @ApiResponse(responseCode = "403", description = "No item found", content = @Content),
          @ApiResponse(responseCode = "404", description = "Tier limit exceeded", content = @Content)})
  public void publish(
          @Parameter(description = "item id to Publish")
          @PathVariable
          String idListing) throws TierLimitException, NoItemFoundException {
    service.publishListings(UUID.fromString(idListing));
  }

  /**
   * UnPublish an existing listing item = change status back to draft
   *
   * @param idListing id of the listing item
   *
   * @throws NoItemFoundException exception if the item is not found
   */
  @ResponseStatus(HttpStatus.ACCEPTED)
  @PutMapping("unpublish/{idListing}")
  @Operation(summary = "UnPublish a existing listing item = change status back to draft")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "202", description = "Item unpublished", content = @Content),
          @ApiResponse(responseCode = "403", description = "No item found", content = @Content),
          @ApiResponse(responseCode = "404", description = "Tier limit exceeded", content = @Content)})
  public void unPublish(
          @Parameter(description = "item id to UnPublish")
          @PathVariable
          String idListing) throws NoItemFoundException {
    service.unPublishListings(UUID.fromString(idListing));
  }

  /**
   * Return liste of all non-published items for the Dealer wanted
   *
   * @param dealerName Dealer name
   *
   * @return liste of all the draft items
   *
   * @throws NoItemFoundException exception if the dealer is not found
   */
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/draft/{dealerName}")
  @Operation(summary = "Return liste of all non-published items for the Dealer wanted")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Found Listings",
                       content = {
                               @Content(mediaType = "application/json", array = @ArraySchema(
                                       schema = @Schema(implementation = Listing.class)
                               ))
                       }),
          @ApiResponse(responseCode = "404", description = "Dealer not found", content = @Content)})
  public List<Listing> getDraftListing(
          @Parameter(description = "Nom dealer")
          @PathVariable
          String dealerName) throws

          NoItemFoundException {
    return (List<Listing>) service.getDealerListings(dealerName, State.DRAFT);
  }

  /**
   * Return liste of all published items for the Dealer wanted
   *
   * @param dealerName Dealer name
   *
   * @return liste of all published items
   *
   * @throws NoItemFoundException exception if the dealer is not found
   */
  @ResponseStatus(HttpStatus.OK)
  @GetMapping("/published/{dealerName}")
  @Operation(summary = "Return liste of all published items for the Dealer wanted")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Found Listings",
                       content = {
                               @Content(mediaType = "application/json", array = @ArraySchema(
                                       schema = @Schema(implementation = Listing.class)
                               ))
                       }),
          @ApiResponse(responseCode = "404", description = "Dealer not found", content = @Content)})
  public List<Listing> getPublishedListing(
          @Parameter(description = "Nom dealer")
          @PathVariable
          String dealerName) throws NoItemFoundException {
    return (List<Listing>) service.getDealerListings(dealerName, State.PUBLISHED);
  }
}
