package com.sm360.assignment.advertising.domain.model;

/**
 * Enum representing the state of the listing item
 */
public enum State {
  /**
   * State unpublished or new - not available online
   */
  DRAFT,

  /**
   * State published - available online
   */
  PUBLISHED

}
