package com.sm360.assignment.advertising.domain.exception;

import com.sm360.assignment.advertising.domain.model.State;

import java.util.Locale;
import java.util.UUID;

public class NoItemFoundException extends RuntimeException {

  public NoItemFoundException(final String dealerName, State state) {
    super(String.format("No item found for dealer: [%s] and state: [%s]", dealerName, state.name().toLowerCase(Locale.ROOT)));
  }

  public NoItemFoundException(final UUID idListing) {
    super(String.format("No item found with id: [%s]", idListing));
  }
}
