package com.sm360.assignment.advertising.domain.model;

import lombok.Builder;

import java.util.UUID;

/**
 * an owner of the advertisement
 *
 * @param id   dealer id
 * @param name dealer name
 */
@Builder
public record Dealer(
        UUID id,
        String name) {
}
