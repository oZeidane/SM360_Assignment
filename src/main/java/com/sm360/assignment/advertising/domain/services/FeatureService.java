package com.sm360.assignment.advertising.domain.services;


import com.sm360.assignment.advertising.domain.model.Feature;
import com.sm360.assignment.advertising.infrastructure.configuration.FeatureProperties;

import java.util.List;
import java.util.Optional;


/**
 * Service to manage the application features
 */
public class FeatureService {

  private final List<Feature> features;

  public FeatureService(final FeatureProperties properties) {
    this.features = properties.getFeatures();
  }

  /**
   * Methode that return if the feature is activated or not
   *
   * @param featureName Feature name
   *
   * @return State of the feature
   */
  public boolean isFeatureActivated(final String featureName) {
    final Optional<Feature> optionalFeature = features.stream()
            .filter(feature -> feature.name().equals(featureName))
            .findFirst();
    return optionalFeature.map(Feature::enabled).orElse(false);
  }
}
