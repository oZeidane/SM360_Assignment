package com.sm360.assignment.advertising.domain.exception;

import com.sm360.assignment.advertising.domain.model.Listing;

public class TierLimitException extends RuntimeException {

  public TierLimitException(final Listing listing) {
    super(String.format("Tier limit exceeded for %s", listing));
  }
}
