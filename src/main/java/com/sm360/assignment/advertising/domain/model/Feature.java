package com.sm360.assignment.advertising.domain.model;

/**
 * Application features record
 *
 * @param name feature name
 * @param enabled feature state
 */
public record Feature(String name, boolean enabled) {
}
