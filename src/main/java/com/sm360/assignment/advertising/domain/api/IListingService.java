package com.sm360.assignment.advertising.domain.api;

import com.sm360.assignment.advertising.domain.exception.NoItemFoundException;
import com.sm360.assignment.advertising.domain.exception.TierLimitException;
import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;

import java.util.Collection;
import java.util.UUID;

/**
 * Domain entry point to functional services
 */
public interface IListingService {


  /**
   * create a listing and store it
   *
   * @param listing listing object
   */
  void createListing(Listing listing);


  /**
   * Update a listing and store it
   *
   * @param listing listing object
   *
   * @throws TierLimitException exception if the tierLimit is reached and the autoTierLimitUnPublish is off
   */
  void updateListing(Listing listing) throws TierLimitException;

  /**
   * Get a list of all the items of a dealer in the wanted state
   *
   * @param dealerName dealer name
   * @param state wanted state
   *
   * @return list of all the items wanted
   *
   * @throws NoItemFoundException exception if the dealer is not found
   */
  Collection<Listing> getDealerListings(String dealerName, State state) throws NoItemFoundException;


  /**
   * Publish a existing listing item = change status to published
   *
   * @param idListing id of the listing item
   *
   * @throws TierLimitException exception if the tierLimit is reached and the autoTierLimitUnPublish is off
   * @throws NoItemFoundException exception if the item is not found
   */
  void publishListings(UUID idListing) throws TierLimitException, NoItemFoundException;


  /**
   * UnPublish a existing listing item = change status back to draft
   *
   * @param idListing id of the listing item
   *
   * @throws NoItemFoundException exception if the item is not found
   */
  void unPublishListings(UUID idListing) throws NoItemFoundException;

}
