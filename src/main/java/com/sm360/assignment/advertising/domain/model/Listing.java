package com.sm360.assignment.advertising.domain.model;

import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * a vehicle advertisement.
 */
@Getter
@Builder
public final class Listing {
  private UUID id;

  @NotNull(message = "The dealer information are mandatory (Id or name)")
  private Dealer dealer;

  @NotBlank(message = "The vehicle is mandatory")
  private String vehicle;

  @Positive(message = "The price should be positive")
  private Double price;

  private LocalDateTime createdAt;

  private LocalDateTime publishedAt;

  private State state;


  /**
   * unpublish the vehicle advertisement
   */
  public void unPublish() {
    this.state = State.DRAFT;
    publishedAt = null;
  }

  /**
   * publish the vehicle advertisement
   */
  public void publish() {
    state = State.PUBLISHED;
    publishedAt = LocalDateTime.now();
  }
}
