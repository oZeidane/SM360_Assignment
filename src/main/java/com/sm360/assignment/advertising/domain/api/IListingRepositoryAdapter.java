package com.sm360.assignment.advertising.domain.api;

import com.sm360.assignment.advertising.domain.exception.NoItemFoundException;
import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * Infrastructure entry point to data storing services
 */
public interface IListingRepositoryAdapter {

  /**
   *  Get stored item
   *
   * @param idListing id of the listing item
   *
   * @return the stored item
   *
   * @throws NoItemFoundException exception if the item is not found
   */
  Listing getById(UUID idListing) throws NoItemFoundException;

  /**
   * Save/update a item
   *
   * @param listing listing item
   */
  void save(Listing listing);

  /**
   * Get a list of all the items of a dealer in the wanted state
   *
   * @param dealerName Dealer name
   * @param state wanted state
   *
   * @return list of all the items wanted
   */
  Collection<Listing> findByDealerNameAndState(String dealerName, State state);

  /**
   * get oldest dealer published listing item
   *
   * @param dealerName Dealer name
   *
   * @return the oldest item
   */
  Listing getOldestDealerPublishedListing(String dealerName);

  /**
   * Get the state of a stored item if it exists
   *
   * @param uuid id of the item
   *
   * @return the state if it exists
   */
  Optional<State> getListingState(UUID uuid);

  /**
   * Get the count of items in a state for a dealer
   *
   * @param state wanted state
   * @param dealerName Dealer name
   *
   * @return the count
   */
  long countByStateAndDealerName(State state, String dealerName);

}
