package com.sm360.assignment.advertising.domain.services;

import com.sm360.assignment.advertising.domain.api.IListingRepositoryAdapter;
import com.sm360.assignment.advertising.domain.api.IListingService;
import com.sm360.assignment.advertising.domain.exception.NoItemFoundException;
import com.sm360.assignment.advertising.domain.exception.TierLimitException;
import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class ListingService implements IListingService {

  public static final String AUTO_TIER_LIMIT_UNPUBLISH_FEATURE_NAME = "autoTierLimitUnPublish";

  private final long tierLimit;
  private final IListingRepositoryAdapter listingRepository;

  private final FeatureService featureService;

  @Override
  public void createListing(final Listing listing) {
    saveUnPublishItem(listing);
  }

  @Override
  public void unPublishListings(final UUID idListing) throws NoItemFoundException {
    Listing listing = listingRepository.getById(idListing);
    saveUnPublishItem(listing);
  }


  @Override
  public void updateListing(final Listing listing) throws TierLimitException {
    checkIfUpdateImpactTierLimit(listing);
    listingRepository.save(listing);
  }


  @Override
  public Collection<Listing> getDealerListings(final String dealerName, State state) throws NoItemFoundException {
    final Collection<Listing> result = listingRepository.findByDealerNameAndState(dealerName, state);

    if (result.isEmpty()) {
      throw new NoItemFoundException(dealerName, state);
    }

    return result;
  }

  @Override
  public void publishListings(final UUID idListing) throws TierLimitException, NoItemFoundException {
    Listing listing = listingRepository.getById(idListing);
    checkTierLimit(listing);

    listing.publish();
    listingRepository.save(listing);
  }

  private void saveUnPublishItem(final Listing listing) {
    listing.unPublish();
    listingRepository.save(listing);
  }

  private void checkTierLimit(final Listing listing) throws TierLimitException {
    final long publishedListingCount = listingRepository.countByStateAndDealerName(State.PUBLISHED, listing.getDealer().name());


    // Check to do if the limit is reached
    if (tierLimit >= publishedListingCount) {

      if (featureService.isFeatureActivated(AUTO_TIER_LIMIT_UNPUBLISH_FEATURE_NAME)) {
        final Listing oldPublishedItem = listingRepository.getOldestDealerPublishedListing(listing.getDealer().name());
        saveUnPublishItem(oldPublishedItem);
      } else {
        throw new TierLimitException(listing);
      }

    }
  }

  private void checkIfUpdateImpactTierLimit(final Listing listing) throws TierLimitException {
    if (State.PUBLISHED.equals(listing.getState())) {
      final Optional<State> stateOfItemBeforeUpdate = listingRepository.getListingState(listing.getId());

      if (stateOfItemBeforeUpdate.isPresent() && stateOfItemBeforeUpdate.get().equals(State.DRAFT))
        checkTierLimit(listing);
    }
  }
}
