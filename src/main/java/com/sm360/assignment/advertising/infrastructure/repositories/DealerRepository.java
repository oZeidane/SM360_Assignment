package com.sm360.assignment.advertising.infrastructure.repositories;

import com.sm360.assignment.advertising.infrastructure.entities.DealerEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface DealerRepository extends CrudRepository<DealerEntity, UUID> {

  Optional<DealerEntity> findByName(String name);


}
