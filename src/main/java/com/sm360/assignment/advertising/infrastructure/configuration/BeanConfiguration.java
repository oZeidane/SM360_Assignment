package com.sm360.assignment.advertising.infrastructure.configuration;

import com.sm360.assignment.advertising.domain.api.IListingRepositoryAdapter;
import com.sm360.assignment.advertising.domain.services.FeatureService;
import com.sm360.assignment.advertising.domain.services.ListingService;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure the dependency injection with Spring in the domain package
 */
@Configuration
public class BeanConfiguration {

  @Bean
  FeatureService featureService(FeatureProperties featureProperties) {
    return new FeatureService(featureProperties);
  }

  @Bean
  ListingService listingService(
          @Value("${assignmentapp.tierlimit}") final long tierLimit,
          final IListingRepositoryAdapter listingRepository,
          final FeatureService featureService) {
    return new ListingService(tierLimit, listingRepository, featureService);
  }

  @Bean
  public OpenAPI customOpenAPI(@Value("${assignmentapp.apiversion}") String appVersion) {
    return new OpenAPI()
            .info(new Info()
                    .title("Advertising API")
                    .contact(new Contact().name("ZEIDANE Oussama").email("zeidane.oussama@gmail.com"))
                    .version(appVersion));
  }

}
