package com.sm360.assignment.advertising.infrastructure.repositories;

import com.sm360.assignment.advertising.domain.api.IListingRepositoryAdapter;
import com.sm360.assignment.advertising.domain.exception.NoItemFoundException;
import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;
import com.sm360.assignment.advertising.infrastructure.entities.ListingEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * Repository implementation that provides data access services in JPA with spring boot data
 */
@Repository
@Transactional
@AllArgsConstructor
public class ListingRepositoryAdapter implements IListingRepositoryAdapter {

  private final ListingRepository listingRepository;
  private final DealerRepository dealerRepository;

  @Override
  public Listing getById(final UUID idListing) throws NoItemFoundException {
    final Optional<ListingEntity> listingEntity = listingRepository.findById(idListing);
    if (listingEntity.isPresent()) {
      return listingEntity.get().toDomain();
    }
    throw new NoItemFoundException(idListing);
  }

  @Override
  public void save(final Listing listing) {
    final ListingEntity entity = ListingEntity.fromDomain(listing);

    if (entity.getDealer().getId() != null) {
      dealerRepository.findById(entity.getDealer().getId())
              .ifPresent(entity::setDealer);

    } else if (entity.getDealer().getName() != null) {
      dealerRepository.findByName(entity.getDealer().getName())
              .ifPresent(entity::setDealer);
    }

    listingRepository.save(entity);
  }

  @Override
  public Collection<Listing> findByDealerNameAndState(final String dealerName, final State state) {
    return listingRepository.findByDealerNameAndState(dealerName, state).stream()
            .map(ListingEntity::toDomain).toList();
  }

  @Override
  public Optional<State> getListingState(final UUID uuid) {
    return listingRepository.findById(uuid).map(ListingEntity::getState);
  }

  @Override
  public Listing getOldestDealerPublishedListing(String dealerName) {
    return listingRepository.getTopByDealerNameOrderByPublishedAtDesc(dealerName).toDomain();
  }

  @Override
  public long countByStateAndDealerName(final State state, final String dealerName) {
    return listingRepository.countByStateAndDealerName(state, dealerName);
  }
}
