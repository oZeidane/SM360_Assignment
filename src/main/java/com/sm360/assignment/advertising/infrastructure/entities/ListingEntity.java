package com.sm360.assignment.advertising.infrastructure.entities;

import com.sm360.assignment.advertising.domain.model.Listing;
import com.sm360.assignment.advertising.domain.model.State;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = "listing")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListingEntity {

  @Id
  @GeneratedValue
  @Type(type = "uuid-char")
  private UUID id;

  @Setter
  @ManyToOne(cascade = CascadeType.ALL)
  private DealerEntity dealer;

  private String vehicle;

  private Double price;

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  private LocalDateTime publishedAt;

  @Builder.Default
  @Enumerated(EnumType.STRING)
  private State state = State.DRAFT;


  public Listing toDomain() {
    return Listing.builder()
            .id(this.id)
            .vehicle(this.vehicle)
            .price(this.price)
            .createdAt(this.createdAt)
            .publishedAt(this.publishedAt)
            .state(this.state)
            .dealer(this.dealer.toDomain())
            .build();
  }

  public static ListingEntity fromDomain(final Listing listing) {
    final ListingEntity build = ListingEntity.builder()
            .id(listing.getId())
            .vehicle(listing.getVehicle())
            .price(listing.getPrice())
            .createdAt(listing.getCreatedAt())
            .publishedAt(listing.getPublishedAt())
            .state(listing.getState())
            .build();

    if (listing.getDealer() != null) {
      build.setDealer(DealerEntity.fromDomain(listing.getDealer()));
    }

    return build;
  }


}
