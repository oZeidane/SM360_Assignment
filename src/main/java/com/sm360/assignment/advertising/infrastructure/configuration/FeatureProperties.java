package com.sm360.assignment.advertising.infrastructure.configuration;

import com.sm360.assignment.advertising.domain.model.Feature;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "assignmentapp")
public class FeatureProperties {

  @Getter
  @Setter
  private List<Feature> features = new ArrayList<>();

}
