package com.sm360.assignment.advertising.infrastructure.entities;

import com.sm360.assignment.advertising.domain.model.Dealer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity(name = "dealer")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DealerEntity {

  @Id
  @GeneratedValue
  @Type(type = "uuid-char")
  private UUID id;

  @Column(unique = true)
  private String name;

  public Dealer toDomain() {
    return Dealer.builder()
            .id(this.id)
            .name(this.name)
            .build();
  }

  public static DealerEntity fromDomain(Dealer dealer) {
    return DealerEntity.builder()
            .id(dealer.id())
            .name(dealer.name())
            .build();
  }

}
