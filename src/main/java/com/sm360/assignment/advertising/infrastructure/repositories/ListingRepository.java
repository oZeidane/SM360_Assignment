package com.sm360.assignment.advertising.infrastructure.repositories;

import com.sm360.assignment.advertising.infrastructure.entities.ListingEntity;
import com.sm360.assignment.advertising.domain.model.State;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.UUID;

public interface ListingRepository extends CrudRepository<ListingEntity, UUID> {

  Collection<ListingEntity> findByDealerNameAndState(String dealerName, State state);

  ListingEntity getTopByDealerNameOrderByPublishedAtDesc(String dealerName);

  long countByStateAndDealerName(State state, String dealerName);

}
