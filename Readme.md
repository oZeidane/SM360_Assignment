# SM360 Backend Tech Assignment Application

A REST service for managing listings for online advertising service

<!-- TOC -->
* [Application architecture](#application-architecture)
* [How to run the app](#how-to-run-the-app)
  * [With maven](#with-maven)
  * [With Java](#with-java)
  * [With Docker](#with-docker)
* [The swagger Documentation](#the-swagger-documentation)
<!-- TOC -->

## Application architecture

This application is a Java Spring boot microservice application that follow the **Hexagonal architecture**, which is a model of designing software applications around domain logic to isolate it from external factors.

<div style="text-align: center">

```mermaid
flowchart  TD

A[/Application layer\] -->|Interfaces| B{{Domain layer}}
B -->|Interfaces| C[\Infrastructure layer/]
C --> D[(Database)]

style A fill:#121097
style B fill:#87898c
style C fill:#309710
```

</div>

We have Java as base language and Spring as the main framework, the **application Layer** use Spring MVC (Web) to expose a Rest api and the **Infrastructure layer** use Spring data jpa with H2 database.

So the power of this architecture is that we can easily switch from an H2 database to a Postgres/mysql/Oracle or other SQL Database but maybe to a nosql database as mongoDb/redis/... , we can also change the **application layer** to use SOAP or even switch to an asynchronous communication with a JMS implementation or AMQP protocol.

All this we can do it without changing our core business logics.

## How to run the app

### With maven

The app use **MAVEN** to build and manage our dependencies, so we can run Maven commands to build and run the app :

`mvn spring-boot:run`

### With Java

- Build the app with maven with the command:

`mvn package`

- after it finish run the command:

`java -jar target/advertising-0.0.1-SNAPSHOT.jar`

> Need to have maven installed !!

> or if not, just download the jar from the [gitlab registry](https://gitlab.com/oZeidane/SM360_Assignment/-/packages) and run the java -jar command


### With Docker

We used the Spring boot maven plugin to build and even publish our app as a Docker image. the image is already built and deployed in gitlab with our CI/CD pipeline.

So just:

- pull the image from **gitlab registry**:

`docker pull registry.gitlab.com/ozeidane/sm360_assignment/advertising:0.0.1-SNAPSHOT`

- and then run :

`docker run -d registry.gitlab.com/ozeidane/sm360_assignment/advertising:0.0.1-SNAPSHOT`

> Need to have docker installed !!

## The swagger Documentation

- run the app

- go to : [http://localhost:8080/api/v1](http://localhost:8080/api/v1)
